<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'humphrey' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k{@,[v^Y3ko5q}|q,R*j}p4eYa6yuUL[Z#}Q.~t.kN~O7+aw1Lp[#|:Wr*r9l,/?' );
define( 'SECURE_AUTH_KEY',  'aCAsZIF./v5E-/h~v@~0Hf>697f47WU#;lr9,S*S>AI<1H7(.-7[Ns*k2sV*=e{$' );
define( 'LOGGED_IN_KEY',    'h5HkxJ-;-_D:7Oy<8^94^*2$aehmW0xuR<mUt&sN5XD6S53RdGnF3zJ,wf(bc$hq' );
define( 'NONCE_KEY',        '7#ie8x[sSQ)al|=d29:qg];7u4izXaG3@fb6Bt6vsqo[6p476R*9{0SY8kr]M0K@' );
define( 'AUTH_SALT',        '`49n_ImN,U|h08TXyX!wZSn/LH(JU_;7AIxdJ#9e9hWGm:4o^dnjPON6p{j^Zi?K' );
define( 'SECURE_AUTH_SALT', '>@`Jxq:}E</XN*3bIPCy)^Uog|<m&:8%8I(vGski#u)bJ%!EVY.gxj9z5=h.Yru}' );
define( 'LOGGED_IN_SALT',   'LKCD)n-Oy.<0Unc_*?s^ar`H1,d[oH}y%Lxyhs9qJ0[x`xS;I*/W2P;QP[FHoP<2' );
define( 'NONCE_SALT',       ']1=~Q:/g.IUstkG%XqHyj!RC&Glv$jSKhhtF2*;nH#9X>)pe]V[UlG64zMZdTBV ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
