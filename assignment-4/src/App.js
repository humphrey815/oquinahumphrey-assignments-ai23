import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import GoogleLogin from 'react-google-login';

class App extends Component {

  render() {

    const responseGoogle = (response) => {
      console.log(response);
    }

    return (
      <div className="App">
        <h1>LOGIN WITH GOOGLE</h1>

      <GoogleLogin
        clientId="694696589941-d033l0c4pr8egmfo0clr3ap66ub2qfp6.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
        buttonText="LOGIN WITH GOOGLE"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />

      </div>
    );
  }
}

export default App;
