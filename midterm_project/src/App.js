import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Result from './Result';
import Button from "./Button";

class App extends Component {
    constructor(){
        super();

        this.state = {
            result: ""
        }
    }

    onClick = button => {

        if(button === "="){
            this.calculate()
        }

        else if(button === "C"){
            this.reset()
        }

        else {
            this.setState({
                result: this.state.result + button
            })
        }
    };


    calculate = () => {
        try {
            this.setState({
                // eslint-disable-next-line
                result: (eval(this.state.result) || "" ) + ""
            })
        } catch (e) {
            this.setState({
                result: "error"
            })

        }
    };

    reset = () => {
        this.setState({
            result: ""
        })
    };

    render() {
        return (
            <div align ="center">
                <div className="calculator-body">
                    <h1>React Calculator</h1>
                    <Result result={this.state.result}/>
                    <Button onClick={this.onClick}/>
                </div>
            </div>
        );
    }
}

export default App;